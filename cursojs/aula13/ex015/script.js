function verificar() {
    var data = new Date()
    var ano = data.getFullYear()
    var fano = document.getElementById('txtano')
    var res = document.querySelector('div#res')
    if (fano.value.length == 0 || Number(fano.value) > ano) {
        window.alert('[ERRO] Verifique os dados e tente novamente.')
    } else {
        var fsex = document.getElementsByName('radsex')
        var idade = ano - Number(fano.value)
        var genero = ''
        var img = document.createElement('img')
        img.setAttribute('id', 'foto')
        if (fsex[0].checked) {
            genero = 'Homem'
            if (idade >= 0 && idade < 10) {
                var desc = 'uma Criança'
                img.setAttribute('src', 'criancahomem.png')
            } else if (idade > 10 && idade < 18) {
                var desc = 'um Jovem'
                img.setAttribute('src', 'jovemhomem.png')
            } else if (idade > 18 && idade <60) {
                var desc = 'um Adulto'
                img.setAttribute('src', 'adultohomem.png')
            } else if (idade > 60) {
                var desc = 'um Idoso'
                img.setAttribute('src', 'idosohomem.png')
            }
        } else if (fsex[1].checked) {
            genero = 'Mulher'
            if(idade >= 0 && idade <10) {
                img.setAttribute('src', 'criancamulher.png')
                var desc = 'uma Criança'
            } else if (idade > 10 && idade < 18) {
                img.setAttribute('src', 'jovemmulher.png')
                var desc = 'uma Jovem'
            } else if (idade > 18 && idade < 60) {
                img.setAttribute('src', 'adultomulher.png')
                var desc = 'uma Adulta'
            } else if (idade > 60) {
                var desc = 'uma Idosa'
                img.setAttribute('src', 'idosomulher.png')
            }
        }
        res.style.textAlign = 'center'
        res.innerHTML = `Detectamos ${desc} ${genero} com ${idade} anos`
        res.appendChild(img)
    }
}