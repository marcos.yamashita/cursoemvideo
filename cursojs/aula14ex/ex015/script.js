function contar() {
    //var inicio = 1
    //var fim = 10
    //passo = 1

    var inicio = document.getElementById('ini')
    var fim = document.getElementById('fim')
    var passo = document.getElementById('passo')
    var res = document.querySelector('div#res')

    //res.innerHTML = `teste ${inicio.value}, ${fim.value}, ${passo.value}`

    if(inicio.value == 0 || inicio.value == '') {
        res.innerHTML = 'Impossivel contar, Verifique os dados!'
    } else {
        if(passo.value == 0 || passo.value == '') {
            window.alert('Passo invalido. Considerando PASSO 1')
            var passo = 1
        } else {
            res.innerHTML = 'Contando...'
            let i = Number(inicio.value)
            let f = Number(fim.value)
            let p = Number(passo.value)
                //res.innerHTML = (`${inicio.value}, ${fim.value}, ${passo.value}`)

                for (c = i; c <= f; c += p) {
                    res.innerHTML += `${c} \u{1F449}` 
                    
                }
                res.innerHTML += `\u{1F3C1}`
            }
        }
    }